extends Control


export (PackedScene) var Counter


signal btn_back_pressed
signal btn_spin_pressed
signal btn_autospin_pressed


# меняется сигналами из GameSlot через Main
var spinning = false
var auto_spinning = false


var score:= 10000.0
#var score_win = 0.0
var level:= 1 # уровень на шкале уровня
var level_score:= 0.0 # выигранные очки
var level_step:= 200 # шаг уровня
var chanse_to_win:= 15 # шанс на победу. условно в процентах
var rate:= 15.0 # повышающий коэффициент при выигрыше

var min_bet:= 0.25 # минимальный порог ставки
var max_bet:= 5.0 # максимальный порог ставки
var bet_step:= 0.25 # шаг инкремента/дикремента
var bet:= 2.5 # текущий коэффициент ставки


# BUILTINS - - - - - - - - -


func _ready() -> void:
	set_level(0.0)


func _process(_delta: float) -> void:
	$Header/Score/Label.text = "$ %s" % str(round(score * 100) / 100)
	$Header/Level/Label.text = "$ %s/%s" % [round(level_score), round(level * level_step)]
	$Footer/Bet/Label.text = "$ %s" % str(round(bet * 100) / 100)
#	$Footer/BgWin/ScoreWin.text = "$ %s" % str(round(score_win * 100) / 100)


# METHODS - - - - - - - - -


# отображение контролов
func show_hide_controls(show: bool) -> void:
	var back_pos: int = 11
	var footer_pos = self.get_rect().size.y - $Footer.get_rect().size.y
	if show:
		$Tween.interpolate_property($Header/BtnBack, "rect_position:y", -100.0, back_pos, 0.2)
		$Tween.interpolate_property($Footer, "rect_position:y", footer_pos + 500.0, footer_pos, 0.3)
	else:
		$Tween.interpolate_property($Header/BtnBack, "rect_position:y", back_pos, -100.0, 0.2)
		$Tween.interpolate_property($Footer, "rect_position:y", footer_pos, footer_pos + 500.0, 0.3)
	if not $Tween.is_active():
		$Tween.start()


# установка очков
func set_score() -> void:
	var counter = Counter.instance()
	if randi() % 100 <= chanse_to_win + chanse_to_win * bet / 2:
		var count = floor(rate * bet + level)
		$Tween.interpolate_property(self, "score", score, score + count, 0.5)
#		$Tween.interpolate_property(self, "score_win", score_win, score_win + count, 0.5)
		set_level(count)
		counter.increase = true
		counter.count = count
	else:
		$Tween.interpolate_property(self, "score", score, score - bet, 0.5)
		counter.increase = false
		counter.count = bet
	get_parent().add_child(counter)
	if not $Tween.is_active():
		$Tween.start()


# установка уровня
func set_level(count: float) -> void:
	var max_width: int = 352
	level_score = level_score + count
	if level_score > level * level_step:
		level_score = level_score - level * level_step
		level += 1
	$Header/IcLevel/Label.text = "%s" % level
	var progress: float = lerp($Header/Level/ProgressBg/ProgressFg.rect_min_size.x, max_width, level_score / (level * level_step))
	$Tween.interpolate_property($Header/Level/ProgressBg/ProgressFg, "rect_size:x", $Header/Level/ProgressBg/ProgressFg.rect_size.x, progress, 0.2)
	$Tween.interpolate_property(self, "level_score", level_score - count, level_score, 0.2)
	if not $Tween.is_active():
		$Tween.start()


# отображение автоспина
func show_hide_autospin_progress(show: bool) -> void:
	var progress_pos: float = ($Footer.get_rect().size.y - $Footer/AutoSpinProgress.get_rect().size.y) / 2
	if show:
		$Tween.interpolate_property($Footer/AutoSpinProgress, "rect_position:y", 200, progress_pos, 0.3)
		$Tween.interpolate_property($Footer/BtnAutoBet, "rect_position:y", 15, 200, 0.3)
		$Tween.interpolate_property($Footer/BtnMaxBet, "rect_position:y", 8, 200, 0.3)
		$Tween.interpolate_property($Footer/Bet, "rect_position:y", 9, 200, 0.3)
		$Tween.interpolate_property($Footer/BtnSpin, "rect_position:y", -24, 200, 0.3)
	else:
		$Tween.interpolate_property($Footer/AutoSpinProgress, "rect_position:y", progress_pos, 200, 0.3)
		$Tween.interpolate_property($Footer/BtnAutoBet, "rect_position:y", 200, 15, 0.3)
		$Tween.interpolate_property($Footer/BtnMaxBet, "rect_position:y", 200, 8, 0.3)
		$Tween.interpolate_property($Footer/Bet, "rect_position:y", 200, 9, 0.3)
		$Tween.interpolate_property($Footer/BtnSpin, "rect_position:y", 200, -24, 0.3)
	if not $Tween.is_active():
		$Tween.start()
	$Footer/AutoSpinProgress/ProgressBg/ProgressFg.rect_size.x = $Footer/AutoSpinProgress/ProgressBg/ProgressFg.rect_min_size.x


func change_autospin_count(auto_spin_count: float, auto_spin_max: float) -> void:
	var max_width: int = 502
	$Footer/AutoSpinProgress/ProgressBg/Label.text = "%s / %s" % [auto_spin_count, auto_spin_max]
	var progress: float = lerp($Footer/AutoSpinProgress/ProgressBg/ProgressFg.rect_min_size.x, max_width, auto_spin_count / auto_spin_max)
	var x:float = $Footer/AutoSpinProgress/ProgressBg/ProgressFg.rect_size.x
	$Tween.interpolate_property($Footer/AutoSpinProgress/ProgressBg/ProgressFg, "rect_size:x", x, progress, 0.2)
	if not $Tween.is_active():
		$Tween.start()


# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_back_pressed")


func _on_BtnSpin_button_down() -> void:
	$TimerSpinLongPress.start()


func _on_BtnSpin_button_up() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_spin_pressed")
		$TimerSpinLongPress.stop()


func _on_Timer_timeout() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)


func _on_BtnMinus_pressed() -> void:
	if not spinning and not auto_spinning:
		var old_bet: float = bet
		bet = bet - bet_step if bet > min_bet else min_bet
		$Tween.interpolate_property(self, "bet", old_bet, bet, 0.05)

		var btn_min_opacity: float = $Footer/Bet/BtnMinus.modulate.a
		var btn_max_opacity: float = $Footer/Bet/BtnPlus.modulate.a
		if bet == min_bet:
			$Tween.interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 0.0, 0.4)
		else:
			$Tween.interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, 0.4)
		$Tween.interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, 0.4)

		if not $Tween.is_active():
			$Tween.start()


func _on_BtnPlus_pressed() -> void:
	if not spinning and not auto_spinning:
		var old_bet: float = bet
		bet = bet + bet_step if bet < max_bet else max_bet
		$Tween.interpolate_property(self, "bet", old_bet, bet, 0.1)

		var btn_max_opacity: float = $Footer/Bet/BtnPlus.modulate.a
		var btn_min_opacity: float = $Footer/Bet/BtnMinus.modulate.a
		if bet == max_bet:
			$Tween.interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 0.0, 0.4)
		else:
			$Tween.interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, 0.4)
		$Tween.interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, 0.4)

		if not $Tween.is_active():
			$Tween.start()


func _on_BtnMaxBet_pressed() -> void:
	if not spinning and not auto_spinning:
		var old_bet: float = bet
		bet = max_bet
		$Tween.interpolate_property(self, "bet", old_bet, bet, 0.1)

		var btn_max_opacity: float = $Footer/Bet/BtnPlus.modulate.a
		var btn_min_opacity: float = $Footer/Bet/BtnMinus.modulate.a
		if bet == max_bet:
			$Tween.interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 0.0, 0.4)
		else:
			$Tween.interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, 0.4)
		$Tween.interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, 0.4)

		if not $Tween.is_active():
			$Tween.start()


func _on_BtnAutoBet_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)
