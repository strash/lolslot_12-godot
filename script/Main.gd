extends Node


onready var MyGUI: Control = get_node("GUI")
onready var MyLevels: Control = get_node("Levels")
onready var MyGame: Control = get_node("Game")


# BUILTINS - - - - - - - - -


func _ready() -> void:
	var _seed = rand_seed(1)

	var _level1_pressed = MyLevels.connect("level1_pressed", self, "_on_MyLevels_level1_pressed")
	var _level2_pressed = MyLevels.connect("level2_pressed", self, "_on_MyLevels_level2_pressed")
	var _level3_pressed = MyLevels.connect("level3_pressed", self, "_on_MyLevels_level3_pressed")

	var _btn_back_pressed = MyGUI.connect("btn_back_pressed", self, "_on_MyGUI_btn_back_pressed")
	var _btn_spin_pressed = MyGUI.connect("btn_spin_pressed", self, "_on_MyGUI_btn_spin_pressed")
	var _btn_autospin_pressed = MyGUI.connect("btn_autospin_pressed", self, "_on_MyGUI_btn_autospin_pressed")

	var _score_increased = MyGame.connect("score_increased", self, "_on_MyGame_score_increased")
	var _spinnig_changed = MyGame.connect("spinnig_changed", self, "_on_MyGame_spinnig_changed")
	var _auto_spinnig_changed = MyGame.connect("auto_spinnig_changed", self, "_on_MyGame_auto_spinnig_changed")
	var _autospin_count_changed = MyGame.connect("autospin_count_changed", self, "_on_MyGame_autospin_count_changed")

	set_view(1)
	MyGUI.show_hide_controls(false)


# METHODS - - - - - - - - -


func set_view(view: int) -> void:
	match view:
		1:
			$Tween.interpolate_property(MyLevels, "modulate:a", 0.0, 1.0, 0.3)
#			MyLevels.show()
			MyGame.hide()
		2:
			$Tween.interpolate_property(MyLevels, "modulate:a", 1.0, 0.0, 0.3)
#			MyLevels.hide()
			MyGame.show()
	if not $Tween.is_active():
		$Tween.start()


# SIGNALS - - - - - - - - -


func _on_MyLevels_level1_pressed() -> void:
	set_view(2)
	MyGame.prepare_level(1)
	MyGUI.show_hide_controls(true)


func _on_MyLevels_level2_pressed() -> void:
	set_view(2)
	MyGame.prepare_level(2)
	MyGUI.show_hide_controls(true)


func _on_MyLevels_level3_pressed() -> void:
	set_view(2)
	MyGame.prepare_level(3)
	MyGUI.show_hide_controls(true)


func _on_MyGUI_btn_back_pressed() -> void:
	set_view(1)
	MyGame.clear_level()
	MyGUI.show_hide_controls(false)


func _on_MyGUI_btn_spin_pressed() -> void:
	MyGame.spin()


func _on_MyGUI_btn_autospin_pressed() -> void:
	MyGame.auto_spin()


func _on_MyGame_score_increased() -> void:
	MyGUI.set_score()


func _on_MyGame_spinnig_changed(status: bool) -> void:
	MyGUI.spinning = status


func _on_MyGame_auto_spinnig_changed(status: bool) -> void:
	MyGUI.auto_spinning = status
	if not status:
		MyGUI.show_hide_autospin_progress(false)


func _on_MyGame_autospin_count_changed(auto_spin_count: float, auto_spin_max: float) -> void:
	MyGUI.change_autospin_count(auto_spin_count, auto_spin_max)
